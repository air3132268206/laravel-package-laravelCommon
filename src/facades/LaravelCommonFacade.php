<?php
namespace  Airfei\LaravelCommon\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Created by PhpStorm.
 * User: air
 * Date: 18-11-29
 * Time: 下午2:08
 */

class LaravelCommonFacade extends Facade{

    protected static function getFacadeAccessor()
    {
        return 'laravelCommon';
    }




}