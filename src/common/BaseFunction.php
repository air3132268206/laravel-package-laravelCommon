<?php
namespace Airfei\LaravelCommon\Common;
use Illuminate\Support\Facades\Route;

/**
 * Created by PhpStorm.
 * User: air
 * Date: 18-11-30
 * Time: 上午11:46
 */

class BaseFunction{
    /**
     * 获取所有路由
     * @return array
     */
    static public function get_all_routes(){
        $route=array();
        foreach (Route::getRoutes() as $key=>$vo){
            $route[$key]['url'] = $vo->uri;       // 路径
            $route[$key]['method'] = implode(',',$vo->methods);   // 方式，get，put，post等 是一个数组
            $route[$key]['prefix'] = $vo->action['prefix'];
        }
        return $route;
    }


    /**
     * 获取当前路由
     * @return mixed
     */
    static public function get_current_route(){
        $route['route']=Route::getCurrentRoute()->uri;
        $route['method']=implode(',',Route::getCurrentRoute()->methods);
        return $route;
    }



}
